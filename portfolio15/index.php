<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bruce McLavy - Interaction Designer & Developer</title>

<link rel='stylesheet' type="text/css" href="css-index.css" title='master' />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    (function(){var p=[],w=window,d=document,e=f=0;p.push('ua='+encodeURIComponent(navigator.userAgent));e|=w.ActiveXObject?1:0;e|=w.opera?2:0;e|=w.chrome?4:0;
    e|='getBoxObjectFor' in d || 'mozInnerScreenX' in w?8:0;e|=('WebKitCSSMatrix' in w||'WebKitPoint' in w||'webkitStorageInfo' in w||'webkitURL' in w)?16:0;
    e|=(e&16&&({}.toString).toString().indexOf("\n")===-1)?32:0;p.push('e='+e);f|='sandbox' in d.createElement('iframe')?1:0;f|='WebSocket' in w?2:0;
    f|=w.Worker?4:0;f|=w.applicationCache?8:0;f|=w.history && history.pushState?16:0;f|=d.documentElement.webkitRequestFullScreen?32:0;f|='FileReader' in w?64:0;
    p.push('f='+f);p.push('r='+Math.random().toString(36).substring(7));p.push('w='+screen.width);p.push('h='+screen.height);var s=d.createElement('script');
    s.src='/BruceMcLavy/port15/whichbrowser/detect.php?' + p.join('&');d.getElementsByTagName('head')[0].appendChild(s);})();

var trayPos = 'top';
var menuOut = false;
var menuToggleState = true;
var trayUpKeyPressed = false;
var trayDownKeyPressed = false;
var trayUpHidden = true;
var trayDownHidden = false;
var imageList;
var currentImage = 0;
var nextImageKeyPressed = false;
var prevImageKeyPressed = false;
var scrollInterval = 0;
var currentProject = 1;
var Browsers;


	$(document).ready(function(){
		function waitForWhichBrowser(cb) {
				var callback = cb;
				
				function wait() {
					if (typeof WhichBrowser == 'undefined') 
						window.setTimeout(wait, 100)
					else 
						callback();
				}
				
				wait();
			}


			waitForWhichBrowser(function() {	
				try {
					Browsers = new WhichBrowser({
						useFeatures:		true,
						detectCamouflage:	true
					});
					if(Browsers.browser.name != "Chrome"){
						$( ".mainContainer").css("border-radius", "15px 15px 0px 0px");
						$( "#overlay").css("border-radius", "15px 15px 0px 0px");
						$( "#instructions").css("display", "none");
					}
				} catch (e) {
					console.log ('Oops, something went wrong with the WhichBrowser feature:' + e);
				}
			});
			
			
			configureLayout();
			loadProjectData();
		
		$( window ).resize(function() {
			configureLayout();
		});
		
		$("#radialMenu").fadeOut(0);
		$("#dimmer").fadeOut(0);
		$("#trayUp").fadeOut(0);
		$("#trayDown").fadeOut(0);		
		
		$("#trayDown").click(function(){
			toggleTrayPos('down');
		});
		
		$("#trayUp").click(function(){
			toggleTrayPos('up');
		});
		
		
		$( "#dots").on("mouseenter", function() {
			if(!trayUpHidden){
				$("#trayUp").fadeIn(150);
				$("#trayUp").css("z-index",4);
			}
			if(!trayDownHidden){
				$("#trayDown").fadeIn(150);
				$("#trayDown").css("z-index",4);
			}
			$("#dots").fadeOut(75);
		});
		
		$( "#trayControl" ).on( "mouseleave", function() {
			$("#trayUp").fadeOut(75);
			$("#trayUp").css("z-index",2);
			$("#trayDown").fadeOut(75);
			$("#trayUp").css("z-index",2);
			$("#dots").fadeIn(150);
		});
		
		$("#trayUp").hover(handleOver, handleOut);
		$("#trayDown").hover(handleOver, handleOut);
			
		window.addEventListener("keydown", checkKeyDown, false);
		window.addEventListener("keyup", checkKeyUp, false);
		window.setInterval(function(){
			scrollInterval = 0;
		},  500);
		
		$(window).on('DOMMouseScroll mousewheel', function (e) {
			if(e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0){
				if(scrollInterval == 0){
					swapImage("prev");
					scrollInterval++;
				}
			} else {
				if(scrollInterval == 0){
					swapImage("next");
					scrollInterval++;
				}				
			}
		});
		
		populateRadialMenu();
	});
	
	function addThumbClick(){
		$(".thumbnail").click(function(){
			var imageInt;
			var thumbID = $(this).attr("id");
			
			if (thumbID == "thumb0"){
				imageInt = 0;
			} else if (thumbID == "thumb1"){
				imageInt = 1;
			} else if(thumbID == "thumb2"){
				imageInt = 2;
			} else if(thumbID == "thumb3"){
				imageInt = 3;
			}
			
			$(".thumbnail").css("transform", "scale(1)");
			$(this).css("transform", "scale(1.2)");			
			setImage(imageInt);
		});
	}
	
	function populateRadialMenu(){
		var webProjectList = [];
		var gamesProjectList = [];
		
		var webMenuList = [$("#leftSectionIcon1"), $("#leftSectionIcon2"),$("#leftSectionIcon3")];
		var gamesMenuList =  [$("#topSectionIcon3"), $("#topSectionIcon2"), $("#topSectionIcon4"), $("#topSectionIcon1"), $("#topSectionIcon5")];
		var proMenuList = [$("#rightSectionIcon1"), $("#rightSectionIcon2"),$("#rightSectionIcon3")];
		var sectionList = [webMenuList, gamesMenuList];
		
		$(".webMenuData").each(function() {
			var thisProject = new project($(this).attr("data-pID"), $(this).attr("data-desc"), $(this).attr("data-icon"), $(this).attr("data-title"), $(this).attr("data-secPosition"),  $(this).attr("data-folder"));
				
			webProjectList.push(thisProject);

		});
		
		$(".gamesMenuData").each(function() {
			var thisProject = new project($(this).attr("data-pID"), $(this).attr("data-desc"), $(this).attr("data-icon"), $(this).attr("data-title"), $(this).attr("data-secPosition"), $(this).attr("data-folder"));
				
			gamesProjectList.push(thisProject);

		});
		
		webProjectList = orderProjectList(webProjectList).slice(0);
		gamesProjectList = orderProjectList(gamesProjectList).slice(0);

		for(var i = 0; i < sectionList.length; i++){
			for(var j = 0; j < sectionList[i].length; j++){
				
				if (i == 0){
					thisList = webProjectList;
				} else if (i == 1){
					thisList = gamesProjectList;
				} else if (i == 2){
					thisList = [];
				}
				
				if (j < thisList.length){
					sectionList[i][j].toggle();
					sectionList[i][j].attr("data-listPos", j);
					sectionList[i][j].attr("data-sec", i );
					sectionList[i][j].html("<img src = 'images/" + thisList[j].folder +"/"+ thisList[j].iconImage + " '/>");
					sectionList[i][j].hover(
						function() {
							var thisSec = $(this).attr("data-sec");
							if (thisSec == 0){
								thisList = webProjectList;
							} else if (thisSec == 1){
								thisList = gamesProjectList;
							} else if (thisSec == 2){
								thisList = [];
							}
							displayMenuInfo(thisList[$(this).attr("data-listPos")].folder + "/" + thisList[$(this).attr("data-listPos")].titleImage, thisList[$(this).attr("data-listPos")].description);
						}, function() { 
								resetMenuInfo() 
						}
					);
					sectionList[i][j].click(function(){
						//newURL = "/BruceMcLavy/port15" + "?pid=" + thisList[$(this).attr("data-listPos")].projectID;
						//history.pushState("somewhereElse", "ThisTitle", newURL);
						var thisSec = $(this).attr("data-sec");
						if (thisSec == 0){
							thisList = webProjectList;
						} else if (thisSec == 1){
							thisList = gamesProjectList;
						} else if (thisSec == 2){
							thisList = [];
						}

						if(currentProject != thisList[$(this).attr("data-listPos")].projectID){
							currentProject = thisList[$(this).attr("data-listPos")].projectID;								
							var xmlhttp = new XMLHttpRequest();
							xmlhttp.onreadystatechange = function() {
								if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
									$("#currentProjectData").html(xmlhttp.responseText);
									loadProjectData();
									toggleRadialMenu();
								}
							}
							xmlhttp.open("GET", "getProjectData.php?pID="+thisList[$(this).attr("data-listPos")].projectID, true);
							xmlhttp.send();																
						} else {
							toggleRadialMenu();
						}
					});
				}else{
					break;
				}
				thisList = [];
			}
		}		
	}
	
	function orderProjectList(thisList){
		var orderedList = [];
		var tempProjectList = thisList.slice(0);
		var removeTemp= 0;
		
		projectPosition = tempProjectList[0];

		while (tempProjectList.length > 0){
			removeTemp = 0;
			for(var i = 0; i < tempProjectList.length; i++){
				if (projectPosition.position < tempProjectList[i].position){
					projectPosition = tempProjectList[i];
					removeTemp = i;
		     	}				
			}
			
			orderedList.push(projectPosition);
			tempProjectList.splice(removeTemp, 1);
			projectPosition = tempProjectList[0];		
		}
		return orderedList;
	}
	
	function displayMenuInfo(menuImage, menuDesc){
		$("#menuProjectTitle").html("<img src = 'images/" + menuImage + " ' />");
		$("#menuProjectDescription").html(menuDesc);
	}
	
	function resetMenuInfo(){
		$("#menuProjectTitle").html("menu_project_title");
		$("#menuProjectDescription").html("menu_project_description");
	}
	
	function project(pID, desc, icon, wordMark, pos, fileLoc){
		this.projectID = pID;
		this.titleImage = wordMark;
		this.iconImage = icon;
		this.description = desc;
		this.position = pos;
		this.folder = fileLoc;
	}
	
	function configureLayout(){
		var containerHeight = $( ".mainContainer" ).height();
		var containerWidth = $( ".mainContainer" ).width();
		var containerPosition = $( ".mainContainer" ).offset();
		var containerCorner = $( ".mainContainer").css("border-radius");
		var trayBodyHeight = $( ".mainContainer" ).height()*.4 - (($("#trayTop").height() + $("#trayTitleBar").height())+20);
		var clipPathString = '<rect x="' + containerPosition.left + '" y="' + containerPosition.top + '" rx="15" ry="15" width="' + (containerWidth+6) + '" height="' + (containerHeight+6) + '" style="fill:red;stroke:black;stroke-width:1;opacity:0.5">'
		
		
		var newWidth = $("#tray").width() -  parseInt($("#trayContent").css("padding-left"))*2;
		
		$("#trayClip").html(clipPathString);				
		$("#trayContent").css({"height": ($("#tray").height() - ($("#trayTop").height() + $("#trayTitleBar").height()))- parseInt($("#trayContent").css("padding-bottom")), "width": newWidth});
		$("#overlay").css({"top" : containerPosition.top, "left" : containerPosition.left, "width" : containerWidth, "height" : containerHeight});
		$("#instructions").css("top",$(".mainContainer").css("top"));
		$("#projectDescription").css("width", $("#trayContent").width());
		newWidth = $("#trayContent").width() - (parseInt($("#projectSpecs").css("padding")) + parseInt($("#projectSpecs").css("border-width")))*2;
		$("#projectSpecs").css("width", newWidth);		
	}
	
	function loadProjectData(){
		$("#projectTitle").html("<img src = 'images/" + $("#projectData").attr("data-folder") +"/"+ $("#projectData").attr("data-image") + " ' />");
		
		$("#projectDescription").html($("#projectData").attr("data-description"));
		
		
		imageList = [$("#projectImages").attr("data-image1"), $("#projectImages").attr("data-image2"), $("#projectImages").attr("data-image3"),$("#projectImages").attr("data-image4")];	
		
		$("#thumbnailsContainer").html('<div class="thumbnail" id="thumb0"> <img src = "images/' + $("#projectData").attr("data-folder") +"/"+ $("#projectThumbs").attr("data-thumb1") + '" /> </div> <div class="thumbnail" id="thumb1"> <img src = "images/' + $("#projectData").attr("data-folder") +"/"+ $("#projectThumbs").attr("data-thumb2") + '" /> </div> <div class="thumbnail" id="thumb2"> <img src = "images/' + $("#projectData").attr("data-folder") +"/"+ $("#projectThumbs").attr("data-thumb3") + '" /> </div> <div class="thumbnail" id="thumb3"> <img src = "images/' + $("#projectData").attr("data-folder") +"/"+ $("#projectThumbs").attr("data-thumb4") + '" /> </div>');
		
		addThumbClick();
		
		$("#techSet").html("<h1>Languages & Applications</h1> <ul> <li>" +  $("#projectTech").attr("data-tech1") + "</li> <li>" + $("#projectTech").attr("data-tech2") + "</li> <li>" + $("#projectTech").attr("data-tech3") + "</li> </ul>");
	
		$("#empSet").html("<h1>Project Emphasis</h1> <ul> <li>" +  $("#projectEmphasis").attr("data-emp1") + "</li> <li>" + $("#projectEmphasis").attr("data-emp2") + "</li> <li>" + $("#projectEmphasis").attr("data-emp3") + "</li> </ul>");
		
		$("#specSet").html("<h1>Project Specs</h1> <ul> <li>" +  $("#projectSpec").attr("data-spec1") + "</li> <li>" + $("#projectSpec").attr("data-spec2") + "</li> <li>" + $("#projectSpec").attr("data-spec3") + "</li> </ul>");
		
		setImage(0);
	}
	
	function setImage(imageInt){
		var thumbNailList = $(".thumbnail");
		for(var i = 0; i < thumbNailList.length; i++){
			var thumbSplit = ($(thumbNailList[i]).attr("id"));
			thumbSplit = thumbSplit.split("thumb");			
			var thumbInt = parseInt(thumbSplit[1]);
			console.log(thumbInt +":"+imageInt);
			if( thumbInt == imageInt){
				break;
			}
		}		
			
		$(".thumbnail").css({"transform": "scale(1)", "border": "1px solid #fff"});
		$(thumbNailList[thumbInt]).css({"transform": "scale(1.2)", "border": "2px solid #f3801f"});	
		currentImage = imageInt;
		$("#imageContainer").html("<img src = 'images/" + $("#projectData").attr("data-folder") +"/"+  imageList[imageInt] + " ' />");
	}
	
	function handleOut(){
		$(this).animate({
			height: "14px",
			width: "28px"
		},75);
	}
	
	function handleOver(){
		var hoverHeight = String($(this).height()*1.15) + "px";
		var hoverWidth = String($(this).width()*1.15) + "px";
		$(this).animate({
			height: hoverHeight,
			width: hoverWidth
		},150);
	}

	function checkKeyDown(e){
		if(e.keyCode == 32 && menuToggleState){
			menuToggleState = false;
			toggleRadialMenu();
		}else if(e.keyCode == 87 && !menuOut){
			trayUpKeyPressed = true;
		}else if (e.keyCode == 83 && !menuOut){
			trayDownKeyPressed = true;
		}else if (e.keyCode == 65 && !menuOut){
			prevImageKeyPressed = true;
		}else if (e.keyCode == 68 && !menuOut){
			nextImageKeyPressed = true;
		}
	}
	
	function checkKeyUp(e){
		if(e.keyCode == 32 && !menuToggleState){
			menuToggleState = true;
			if(menuOut){				
				toggleRadialMenu();
			}
		}else if(e.keyCode == 87 && trayUpKeyPressed){
			trayUpKeyPressed = false;
			toggleTrayPos('up');
		}else if (e.keyCode == 83 && trayDownKeyPressed){
			trayDownKeyPressed = false;
			toggleTrayPos('down');
		}else if (e.keyCode == 65 && prevImageKeyPressed){
			swapImage("prev");
		}else if (e.keyCode == 68 && nextImageKeyPressed){
			swapImage("next");
		}
	}
	
	function swapImage(swapWith){
		if (swapWith == "next"){
			getImage = currentImage - 1;
			if (getImage < 0){
				getImage = 3;
			}
		} else if (swapWith == "prev"){
			getImage = currentImage + 1;
			if(getImage > 3){
				getImage = 0;
			}
		}
		setImage(getImage);
	}
	
	function toggleRadialMenu(){
		if(menuOut){
			$("#radialMenu").fadeOut(150);
			$("#dimmer").fadeOut(250);
			menuOut = false;
		}else{
			$("#radialMenu").fadeIn(150);
			$("#dimmer").fadeIn(250);
			menuOut = true;
		}
	}
	
	function toggleTrayPos(toggleDirection){
		var bottomPos = String(100-(($("#trayTop").height()+3)/$( ".mainContainer" ).height())*100) + '%';
		var midPos = String(100-(($("#trayTitleBar").height()+5)/$( ".mainContainer" ).height())*100) + '%';
		var topPos = '60%';
		
		if(toggleDirection == 'up'){
			if(trayPos == 'bottom'){
				$("#trayUp").css({"top":"auto", "bottom":"-50%", "-webkit-transform": "translateY(-100%)", "-ms-transform": "translateY(-100%)",  "transform":"translateY(-100%)"});
				$("#tray").animate({top: midPos});
				trayUpHidden = false;
				trayDownHidden = false;
				trayPos = 'mid';
			}else if (trayPos == 'mid'){
				$("#trayDown").css({"top":"50%","-webkit-transform": "rotate(180deg) translateY(50%)", "-ms-transform": "rotate(180deg) translateY(50%)",  "transform":"rotate(180deg) translateY(50%)"});
				$("#tray").animate({top: topPos});
				$("#trayUp").hide();				
				trayUpHidden = true;
				trayPos = 'top';
			}			
		}else if (toggleDirection == 'down'){						
			if (trayPos == 'top'){
				$("#trayDown").css({"top" : "0px", "-webkit-transform": "rotate(180deg)", "-ms-transform": "rotate(180deg)", "transform": "rotate(180deg)"});
				$("#tray").animate({top: midPos});				
				trayDownHidden = false;
				trayUpHidden = false;
				trayPos = 'mid';
			}else if (trayPos == 'mid'){
				$("#trayUp").css({"top":"50%", "bottom" : "auto","-webkit-transform": "translateY(-50%)", "-ms-transform": "translateY(-50%)",  "transform":"translateY(-50%)"});
				$("#tray").animate({top: bottomPos});
				$("#trayDown").hide();
				trayDownHidden = true;
				trayPos = 'bottom';				
			}
		}
	}
	
	/*function setTrayMask(){
		var trayBodyHeight = $( ".mainContainer" ).height()*.4 - ($("#trayTop").height() + $("#trayTitleBar").height());
		var containerHeight = $( ".mainContainer" ).height()*.4 - ($("#trayTop").height() + $("#trayTitleBar").height());
		var containerWidth = $( ".mainContainer" ).width();
		var containerPosition = $( ".mainContainer" ).offset();
		var containerCorner = $( ".mainContainer").css("border-radius");
		$("#trayBody").css("height",trayBodyHeight);		
	}*/
	
</script>

</head>

<body>
	<div id="portfolioLogo">
    	<img src="images/logo.png" />
    </div>
    
    <div id="overlay"></div>
    
	<div class="mainContainer">
    	<div id="radialMenu">
        	<div id="menuProjectTitle">menu_project_title</div>
            <div id="menuProjectDescription">menu_project_description</div>
            <div id="topSectionIcon1" class="menuIcon button"></div>
            <div id="topSectionIcon2" class="menuIcon button"></div>
            <div id="topSectionIcon3" class="menuIcon button"></div>
            <div id="topSectionIcon4" class="menuIcon button"></div>
            <div id="topSectionIcon5" class="menuIcon button"></div>
            
            <div id="leftSectionIcon1" class="menuIcon button"></div>
            <div id="leftSectionIcon2" class="menuIcon button"></div>
            <div id="leftSectionIcon3" class="menuIcon button"></div>
            
            <div id="rightSectionIcon1" class="menuIcon button"></div>
            <div id="rightSectionIcon2" class="menuIcon button"></div>
            <div id="rightSectionIcon3" class="menuIcon button"></div>
            
        </div>
        <div id="dimmer"></div>
        <div class="menuIconsContainer">
        	<div id="radialMenuButton" class="menuIcon" onclick="toggleRadialMenu()"></div>
        	<div class="menuIcon"></div>
        </div>
	    <div id="tray">
        	<div id="trayTop">
            	<div id="trayControl">
                	<div id="dots"></div>
                	<div id="trayUp"></div>
                    <div id="trayDown"></div>
                </div>
            </div>
			<div id="trayTitleBar">
            	<div id="projectTitle">project_title_image</div> 
	        	<div id="thumbnailsContainer">
                	<div class="thumbnail" id="thumb0"> thumb 0</div>
	                <div class="thumbnail" id="thumb1"> thumb 1</div>
    	            <div class="thumbnail" id="thumb2"> thumb 2</div>
        	        <div class="thumbnail" id="thumb3"> thumb 3</div>
            	 </div>              
            </div>
     		<div id="trayContent">
	            <div id="projectDescription">project_description</div>
    	        <div id="projectSpecs">
                	<div id="specSet" class="skillSet">
                    	<h1>Project Specs</h1>
                        <ul>
                        	<li>item 1</li>
	                        <li>item 2</li>
    	                    <li>item 3</li>
                        </ul>
                    </div>
                    
                    <div id="techSet" class="skillSet">
                    	<h1>Languages & Applications</h1>
                        <ul>
                        <li>item 1</li>
                        <li>item 2</li>
                        <li>item 3</li>
                        </ul>
                    </div>
                    
                    <div id="empSet" class="skillSet">
                    	<h1>Project Emphasis</h1>
                        <ul>
                        	<li>item 1</li>
	                        <li>item 2</li>
     	                   <li>item 3</li>
                        </ul>                        
                    </div>
                    
                </div>            
            </div>
        </div>
        <div id="imageContainer"> image container </div>        
	</div>
    <div id="instructions">
    Space bar = menu  "W" = Raises info tray  "S"= Lowers info tray "A"/Mouse wheel scroll-up = Next image  "D"/Mouse wheel scroll-down = Previous image
    </div>
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<defs>
			<clipPath id="trayClip">
  				<rect x="50" y="20" rx="15" ry="15" width="150" height="150" style="fill:red;stroke:black;stroke-width:1;opacity:0.5" />
			</clipPath>
		</defs>
	</svg>
    <div id="menuProjectData">
    <?php getProjects(); ?>
    </div>
    <div id="currentProjectData">
    <?php
	$projectId; 
	if (empty($_GET['pid'])){
		$projectId = 1;
	} else {
		$projectId = $_GET['pid'];		
	}
	getProjectData($projectId); ?>
    </div>
    
    
</body>
</html>

<?php
$project;
$projectDesc;
$titleImage;

function getProjects(){
	$hostname = "ngassault.db.3606707.hostedresource.com";
    $username = "ngassault";
    $dbname = "ngassault";
	
	$password = "northernGA1!";
    $usertable = "projects";
	
	// Create connection
	$conn = new mysqli($hostname, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
    	die("Connection failed: " . $conn->connect_error);
	}
	
	$sql = "SELECT project_Id, project, short_desc, title_image, menu_icon, secPosition, projFolder  FROM projects WHERE section = 'web'";
	
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			echo '<div class = "webMenuData" data-pID = "' . $row["project_Id"] . '"data-project = "' . $row["project"] . '" data-desc = "' . $row["short_desc"] . '" data-title ="'. $row["title_image"] . '" data-icon ="' . $row["menu_icon"] . '" . " data-secPosition="' . $row["secPosition"] . '"data-folder="' .$row["projFolder"]. '"> </div>';
		}		
	}
	
	$result = null;
	$sql = "SELECT project_Id,project, short_desc, title_image, menu_icon, secPosition, projFolder FROM projects WHERE section = 'games'";
	
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			echo '<div class = "gamesMenuData" data-pID = "' . $row["project_Id"] . '"data-project = "' . $row["project"] . '" data-desc = "' . $row["short_desc"] . '" data-title ="'. $row["title_image"] . '" data-icon ="' . $row["menu_icon"] . '" . " data-secPosition="' . $row["secPosition"] . '" data-folder="' .$row["projFolder"]. '"> </div>';
		}		
	}
	
	$result = null;
	$sql = "SELECT project_Id, project, short_desc, title_image, menu_icon, secPosition, projFolder FROM projects WHERE section = 'pro'";
	
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			echo '<div class = "proMenuData"  data-pID = "' . $row["project_Id"] . '"data-project = "' . $row["project"] . '" data-desc = "' . $row["short_desc"] . '" data-title ="'. $row["title_image"] . '" data-icon ="' . $row["menu_icon"] . '" . " data-secPosition="' . $row["secPosition"] . '"data-folder="' .$row["projFolder"]. '"> </div>';
		}		
	}
	
	$conn->close();
	
}


function getProjectData($projectID){
	global $project, $projectDesc, $titleImage;
	$hostname = "ngassault.db.3606707.hostedresource.com";
    $username = "ngassault";
    $dbname = "ngassault";
	
	$password = "northernGA1!";
    $usertable = "projects";
	
	// Create connection
	$conn = new mysqli($hostname, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
    	die("Connection failed: " . $conn->connect_error);
	}
	
	$sql = "SELECT project_Id, project, long_desc, title_image, projFolder FROM projects WHERE project_Id = '$projectID'";
	
	$result = $conn->query($sql);
	
	if ($result->num_rows > 0) {
     	while($row = $result->fetch_assoc()) {			
			echo '<div id="projectData" data-pID = "' .$row["project_Id"]. '"data-title = "'. $row["project"]. '" data-description = "'. $row["long_desc"].'" data-image ="'. $row["title_image"] . '"data-folder="' .$row["projFolder"]. '"> </div>';
     	}
	} else {
     	echo "0 results";
	}
	
	$result = null;
	$sql = "SELECT type, value FROM projectData WHERE project_Id = '$projectID'";
	
	$thumbnailList = array();
	$imageList = array();
	$empList = array();
	$techList = array();
	$specList = array();
	
	$result = $conn->query($sql);
	
	if ($result->num_rows > 0) {
     	while($row = $result->fetch_assoc()) {			
			if($row["type"] == "thumbnail"){
				$thumbnailList[] = $row["value"];
			}else if($row["type"] == "image"){
				$imageList[] = $row["value"];				
			}else if($row["type"] == "emphasis"){
				$empList[] = $row["value"];
			}else if($row["type"] == "tech"){
				$techList[] = $row["value"];
			}else if($row["type"] == "spec"){
				$specList[] = $row["value"];
			}
     	}
	} else {
     	echo "0 results";
	}
	
	echo '<div id="projectThumbs" data-thumb1 = "' . $thumbnailList[0] . '" data-thumb2 = "' . $thumbnailList[1] . '" data-thumb3 ="'. $thumbnailList[2] . '" data-thumb4 ="' . $thumbnailList[3] . '"> </div>';
	echo '<div id="projectImages" data-image1 = "' . $imageList[0] . '" data-image2 = "' . $imageList[1] . '" data-image3 ="'. $imageList[2] . '" data-image4 ="' . $imageList[3] . '"> </div>';
	echo '<div id="projectEmphasis" data-emp1 = "' . $empList[0] . '" data-emp2 = "' . $empList[1] . '" data-emp3 ="'. $empList[2] . '"> </div>';
	echo '<div id="projectSpec" data-spec1 = "' . $specList[0] . '" data-spec2 = "' . $specList[1] . '" data-spec3 ="'. $specList[2] . '"> </div>';
	echo '<div id="projectTech" data-tech1 = "' . $techList[0] . '" data-tech2 = "' . $techList[1] . '" data-tech3 ="'. $techList[2] . '"> </div>';
	
	$result = null;
	
	$conn->close();	
}
?>