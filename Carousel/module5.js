var module5 = new contentModule("5 - WIP: Breakout Remake", module5Content, module5Clear);

var gameLoop2;

function module5Content(){
	if(module5.active){
		console.log(this.name + " is ACTIVE");
		var game = new gameStateManager();
		var score = 0;
		
		var paddleImg = new Image();
		var paddleX = 15;
		var paddleY = 0; 
		var paddleWidth = 16;
		var paddleHeight = 65; 
		var paddleDeltaX = 0;
		var paddleDeltaY = 10;
		var paddleMaxClimbSpeed = -2;
		var paddleCurrentSpeed = 0;
		var paddleMaxDropSpeed = 2;
		var paddleMove = false;
		
		var rayLength =2;
		
		var ball = {
			posX: 300,
			posY: 240,
			radius: 8,
			minDeltaX: 2,
			maxDeltaX:12,
			minDeltaY:2,
			maxDeltaY: 12,
			bounced: false,
			elasticity: .75,
			dx: 4,
			dy: 4
		};
		
		var gravity = 0.03;
		var drag = 0.999;
		
		var scoreboardWidth = canvas.width;
		var scoreboardHeight = 55;
		var scoreboardY = canvas.height - scoreboardHeight;			
		var scoreGradient = ctx.createLinearGradient(0, scoreboardY-15, 0 ,  scoreboardY+scoreboardHeight);
		var updateScore = false;
		
		var lifeXPos = 85;
		var lifeYPos = canvas.height-40;
		var lifeIconWidth = paddleHeight * .5;
		var lifeIconHeight = paddleWidth * .5;
		
		var lives = 3;
		var paused = true;
		var gameover = true;
		
		// Brick Layout: 1 is orange, 2 is green, 3 is gray, 0 means no brick 
		var bricks = [
			[1,1,1,1,1,1],
			[2,1,2,2,1,2],
			[2,2,1,1,2,2],
			[2,3,1,1,3,2],
			[3,1,1,1,1,3]
		];
		
		var level1Bricks = [
			["a1","c2","a1","a1","c2","a1"],
			["b2","a1","a3","a3","a1","b2"],
			["a4","e2","b3","b3","e2","a4"],
			["b3","d2","e4","e4","d2","b3"],
			["a1","c1","b1","b1","c1","a1"]
		]
		
		var currentBrickSet =[];
				
		var	bricksPerColumn = bricks[0].length;                               
		var brickWidth = 25;
		var brickHeight = (canvas.height-scoreboardHeight+1)/bricksPerColumn;
		
		game.init();
		
		function Brick(brickType, brickHealth){
			console.log("creating brick");
			this.health = brickHealth;
			//this.shape = 
			this.type = brickType;
			this.colour = getBrickColor;
			
			var aColour = ["98e0f9", "7cd8f7","60cff6", "44c7f4"];
			var	bColour = ["c9ecfc", "bae6fb", "ace1fa", "9ddcf9"];				
			var	cColour = ["9da1b4", "82869f", "686d8a", "4d5376"];
			var	dColour = ["f7a486", "f58b64", "f37343", "f15a22"];
			var	eColour = ["fbc483", "f9b360", "f8a33f", "f7931d"];
			
			function getBrickColor(){
				
				var colourType;
				var colour;
				
				console.log("type: " + this.type);
				
				if (this.type == "a"){
					colourType = aColour;
				} else if (this.type == "b"){
					colourType = bColour;
					console.log("colour set: " + colourType);
				} else if (this.type == "c"){
					colourType = cColour;
				} else if (this.type == "d"){
					colourType = dColour;
				} else if (this.type == "e"){
					colourType = eColour;
				}
				
				colour = "#" + colourType[this.health-1];

				return colour;
			}
		}
		
		function drawScoreboard(){

			scoreGradient.addColorStop(0, "black");
			scoreGradient.addColorStop(0.05, "black");
			scoreGradient.addColorStop(0.3, "#c6cbe4");
			scoreGradient.addColorStop(0.95, "#9aa0c4");
			scoreGradient.addColorStop(1, "black");
			
			ctx.fillStyle = scoreGradient;
			ctx.fillRect(0, scoreboardY+1, scoreboardWidth, scoreboardHeight);
			
			ctx.font = "bold 18px Verdana";
			
			if (updateScore){
				ctx.fillStyle = "red";
			} else {
				ctx.fillStyle = 'black';
			}
			ctx.fillText('Score: '+score,10,canvas.height-10);
			
			ctx.fillStyle = 'black';
			ctx.fillText('Lives: ',10,canvas.height-30);
			
			lifeXPos = 72;
			
			for (var i=0; i <lives; i++) {
				ctx.fillRect(lifeXPos, lifeYPos, lifeIconWidth, lifeIconHeight);
				lifeXPos += lifeIconWidth + 15; 
			}
				
		}
		
		function updateBall(){
			ctx.clearRect(ball.posX - (ball.radius+1), ball.posY - (ball.radius+1), ball.radius*2+2, ball.radius*2+2);
			moveBall();
			ctx.fillStyle = '#989898';
			ctx.beginPath();
			ctx.arc(ball.posX,ball.posY,ball.radius,0,Math.PI*2,true);
			ctx.fill();			 
		}
		
		function moveBall(){	
			var rayCheckX = (ball.dx / Math.abs(ball.dx)) *rayLength;
			var rayCheckY = 2;
			if (ball.dy != 0){
				rayCheckY = (ball.dy / Math.abs(ball.dy)) *rayLength;
			}
			
			//check farwall
			if (ball.posX + rayCheckX + ball.radius > canvas.width || collisionXWithBricks()){  
				ball.dx = -(ball.dx * ball.elasticity);
			}
			
			//check collision on tops and bottoms.	
			if (ball.posY + rayCheckY - ball.radius < 0 || ball.posY + rayCheckY + ball.radius > canvas.height-scoreboardHeight || collisionYWithBricks()){
				ball.dy = -(ball.dy * ball.elasticity);
			}

			//console.log(ball.posY + rayCheckY +"> " + paddleY + paddleHeight);
			if(ball.posX + rayCheckX - ball.radius <= paddleX+paddleWidth){			
				if(ball.posX + rayCheckX + ball.radius >= paddleX+paddleWidth-4){
					if(ball.posY + rayCheckY >= paddleY && ball.posY + rayCheckY <= paddleY+paddleHeight && !ball.bounced){
						if(ball.posY + rayCheckY >= paddleY && ball.posY + rayCheckY <= paddleY + paddleHeight * 0.15){
							ball.dy = -4;
						}else if(ball.posY + rayCheckY <= paddleY + paddleHeight && ball.posY+ball.dy >= paddleY+paddleHeight - paddleHeight*0.15){
							ball.dy = 4;
						}
						
						ball.bounced = true;
						ball.dx =- (ball.dx * 2.25);
					}
				} else if(ball.posX + rayCheckX - ball.radius >= paddleX && !ball.bounced) {
					if(ball.posY + rayCheckY + ball.radius >= paddleY && ball.posY + rayCheckY - ball.radius <= paddleY+paddleHeight){
						ball.bounced = true;
						ball.dy =- (ball.dy * 1.75);
					}
				}
								
			}
			if (ball.posX-ball.radius > paddleX+paddleWidth+1){ 
				ball.bounced = false;
			}
			
			if (ball.posX + ball.dx - ball.radius < 0){
				game.lost();
			}
			
			ball.dy += gravity;
			
			ball.dx *= drag;
			ball.dy *= drag;
			
			if(Math.abs(ball.dx) > ball.maxDeltaX){
				if(ball.dx > 0){
					ball.dx  = ball.maxDeltaX;
				} else {
					ball.dx = -ball.maxDeltaX;
				}
			}
			

			  
			if(Math.abs(ball.dx) < ball.minDeltaX){
				if(ball.dx > 0){
					ball.dx  = ball.minDeltaX;
				} else {
					ball.dx = -ball.minDeltaX;
				}
			}

			if(ball.posY + ball.dy + ball.radius > canvas.height-scoreboardHeight && (ball.dy < 1 && ball.dy > -1)){
				ball.dy = 0;
			}
			
			ball.posX += ball.dx;
			ball.posY += ball.dy;
						
			//console.log("x: " + ball.dx + ", y: " + ball.dy);	
		}
		
		function initializeBricks(thisBrickSet){
			currentBrickSet = [];
			for (var i=0; i < thisBrickSet.length; i++) {
				var newColumn = [];
				for (var j=0; j< thisBrickSet[i].length; j++) {
					var brickInfo = thisBrickSet[i][j].split("");
					newColumn.push(new Brick(brickInfo[0], brickInfo[1]));
				}
				currentBrickSet.push(newColumn);
			}		
		}
		
		function createBricks(){
			for (var i=0; i < currentBrickSet.length; i++) {
				for (var j=0; j< currentBrickSet[i].length; j++) {	
					//console.log("brick:" + currentBrickSet[i][j].type);				
					drawBrick(i,j,currentBrickSet[i][j]);
				}
			}
		}
		
		function drawBrick(x,y,brick){			
			if(brick.health){
				//console.log("this colour: " + brick.colour());
				ctx.fillStyle = brick.colour();
				ctx.fillRect(canvas.width - (currentBrickSet.length - x) * brickWidth,y*brickHeight,brickWidth,brickHeight);
				ctx.strokeRect((canvas.width - (currentBrickSet.length - x) * brickWidth)+1,y*brickHeight + 1,brickWidth-2,brickHeight-2);
			} else {
				ctx.clearRect(canvas.width - (currentBrickSet.length - x) * brickWidth ,y*brickHeight,brickWidth,brickHeight);
			}
		}
		
		function collisionXWithBricks(){ 
			var bumpedX = false;
			
			for (var i=0; i < currentBrickSet.length; i++) {
				for (var j=0; j < currentBrickSet[i].length; j++) {
					if (currentBrickSet[i][j].health){ // if brick is still visible
						var brickX = canvas.width - (currentBrickSet.length - i) * brickWidth;
						var brickY = j * brickHeight;

						if ((
						(ball.posX + ball.dx + ball.radius >= brickX) 
						&& 
						(ball.posX + ball.radius <= brickX))
						||
						((ball.posX + ball.dx - ball.radius <= brickX + brickWidth) 
						&&
						(ball.posX - ball.radius >= brickX + brickWidth)))
						{      
							if ((ball.posY + ball.dy -ball.radius <= brickY + brickHeight) 
							&&
							(ball.posY + ball.dy + ball.radius >= brickY))
							{
								explodeBrick(i,j);		 
								bumpedX = true;
							}
						}
					}
				}
    		}
	        return bumpedX;
		}
		
		function collisionYWithBricks(){
			var bumpedY = false;
			for (var i=0; i < currentBrickSet.length; i++) {
				for (var j=0; j < currentBrickSet[i].length; j++) {
					if (currentBrickSet[i][j].health){ // if brick is still visible
						var brickX = canvas.width - (currentBrickSet.length - i) * brickWidth;
						var brickY = j * brickHeight;						
						
				        if ((
						(ball.posY + ball.dy - ball.radius <= brickY + brickHeight) 
						&& 
						(ball.posY - ball.radius >= brickY + brickHeight))
						||
						((ball.posY + ball.dy + ball.radius >= brickY)
						&&
						(ball.posY + ball.radius <= brickY )))
						{
							if ( ball.posX + ball.dx + ball.radius >= brickX 
							&& 
							ball.posX + ball.dx - ball.radius <= brickX + brickWidth)
							{                                  
								explodeBrick(i,j);                          
								bumpedY = true;
							}                       
						}
					}
				}
			}
			return bumpedY;
		}
		
		function explodeBrick(i,j){
		
			currentBrickSet[i][j].health--;
					
			if (currentBrickSet[i][j].health){ 
				score++;
			} else {
				score += 2;   
			}
			updateScore = true;
			createBricks();

			setTimeout(function(){ updateScore = false;}, 5000);
		}
		
		/*function createBricks(){
			for (var i=0; i < bricks.length; i++) {
				for (var j=0; j< bricks[i].length; j++) {
					drawBrick(i,j,bricks[i][j]);
				}
			}
		}
		
		function drawBrick(x,y,type){   
			switch(type){ // set brick colour if visible, clear if not.
				case 1:
					ctx.fillStyle = 'orange';
					break;
				case 2:
					ctx.fillStyle = 'rgb(100,200,100)';                     
					break;
				case 3:
					ctx.fillStyle = 'rgba(50,100,50,.5)';
					break;                              
				default:
					ctx.clearRect(canvas.width - (bricks.length - x) * brickWidth ,y*brickHeight,brickWidth,brickHeight);
					break;
		
			}
			if (type){
				//Draw rectangle with fillStyle color selected earlier w/ border
				//console.log("x: " + canvas.width - (bricks.length - x) * brickWidth + ", y: " + y*brickHeight + " > " + brickWidth + " , " + brickHeight);
				ctx.fillRect(canvas.width - (bricks.length - x) * brickWidth,y*brickHeight,brickWidth,brickHeight);
				ctx.strokeRect((canvas.width - (bricks.length - x) * brickWidth)+1,y*brickHeight + 1,brickWidth-2,brickHeight-2);
			} 
		}
		
		function collisionXWithBricks(){ 
			var bumpedX = false;
			
			for (var i=0; i < bricks.length; i++) {
				for (var j=0; j < bricks[i].length; j++) {
					if (bricks[i][j]){ // if brick is still visible
						var brickX = canvas.width - (bricks.length - i) * brickWidth;
						var brickY = j * brickHeight;

						if ((
						(ball.posX + ball.dx + ball.radius >= brickX) 
						&& 
						(ball.posX + ball.radius <= brickX))
						||
						((ball.posX + ball.dx - ball.radius <= brickX + brickWidth) 
						&&
						(ball.posX - ball.radius >= brickX + brickWidth)))
						{      
							if ((ball.posY + ball.dy -ball.radius <= brickY + brickHeight) 
							&&
							(ball.posY + ball.dy + ball.radius >= brickY))
							{
								explodeBrick(i,j);		 
								bumpedX = true;
							}
						}
					}
				}
    		}
	        return bumpedX;
		}
		
		function collisionYWithBricks(){
			var bumpedY = false;
			for (var i=0; i < bricks.length; i++) {
				for (var j=0; j < bricks[i].length; j++) {
					if (bricks[i][j]){ // if brick is still visible
						var brickX = canvas.width - (bricks.length - i) * brickWidth;
						var brickY = j * brickHeight;
						
						
				        if ((
						(ball.posY + ball.dy - ball.radius <= brickY + brickHeight) 
						&& 
						(ball.posY - ball.radius >= brickY + brickHeight))
						||
						((ball.posY + ball.dy + ball.radius >= brickY)
						&&
						(ball.posY + ball.radius <= brickY )))
						{
							if ( ball.posX + ball.dx + ball.radius >= brickX 
							&& 
							ball.posX + ball.dx - ball.radius <= brickX + brickWidth)
							{                                  
								explodeBrick(i,j);                          
								bumpedY = true;
							}                       
						}
					}
				}
			}
			return bumpedY;
		}
		
		function explodeBrick(i,j){
			if (bricks[i][j] == 3){
				ball.dx = ball.dx * 3;
			} else if (bricks[i][j] == 2){
				ball.dx = ball.dx / 2;
			} else if (bricks[i][j] == 1){
				ball.dx = ball.dx * 1.5;
			}
			
			bricks[i][j] --;
					
			if (bricks[i][j]>0){ 
				score++;
			} else {
				score += 2;   
			}
			updateScore = true;
			createBricks();

			setTimeout(function(){ updateScore = false;}, 5000);
		}*/
		
		function updatePaddle(){
			ctx.clearRect(paddleX,0, paddleWidth, canvas.height-(scoreboardHeight));
			movePaddle();
			ctx.fillStyle = "black";
			ctx.fillRect(paddleX, paddleY, paddleWidth, paddleHeight);
		}
		
		function movePaddle(){
			if(paddleMove && paddleCurrentSpeed > paddleMaxClimbSpeed){
				paddleCurrentSpeed -= .02;
			} else if (paddleCurrentSpeed < paddleMaxDropSpeed){
				paddleCurrentSpeed += .02;				
			}
			
			if (paddleY + paddleHeight + (paddleDeltaY * paddleCurrentSpeed)  > canvas.height-(scoreboardHeight)){
				paddleCurrentSpeed = 0;
			}
			
			if (paddleY + (paddleDeltaY * paddleCurrentSpeed)  < 0){
				paddleCurrentSpeed = 0;
			}
			
			paddleY = paddleY + (paddleDeltaY * paddleCurrentSpeed);
		}
		
		
		
		function gameStateManager(){
			this.init = gameInit;
			this.start = gameStart;
			this.pause = gamePause;
			this.over = gameOver;
			this.win = gameWin;
			this.lost = lostLife;
			
			function gameInit(){
				score = 0;
				paused = true;
				gameover = false;
				
				ball.posX = 300;
				ball.posY = 240;
				ball.dx =6;
				ball.dy = 4;
				
				initializeBricks(level1Bricks);	
							
				bricksPerColumn = currentBrickSet[0].length;   
							
				lives = 3;
				$(document).keydown(function(evt) {
					if (evt.keyCode == 32) {
						if (gameover){
							game.init();
						}else if(paused){
							game.start();
						} else {
							paddleMove = true;
						}
					} //else if (evt.keyCode == 37){
						//paddleMove = 'LEFT';
				});
				
				$(document).keyup(function(evt) {
					if (evt.keyCode == 32) {
							paddleMove = false;
					} //else if (evt.keyCode == 37){
						//paddleMove = 'LEFT'; 
				});
				//initGraphics();
				gameStart();
			}
			
			function gameStart(){
				console.log ("Starting Game");
				paused = false;
				ctx.clearRect(0, 0, canvas.width, canvas.height);
				
				createBricks();
				gameLoop2 = setInterval(animateGame, 16);
			}
				
			function gamePause(){
				clearInterval(gameLoop2);
			}
			
			function gameOver(){
				console.log("gameOver!");
				gameover = true;
								
					ctx.fillStyle = "black";
					ctx.fillRect(canvas.width/2 -150 , canvas.height/2 -37 , 300, 75);
					
					ctx.fillStyle = 'white';
					ctx.fillText('GAME OVER', canvas.width/2 - 135, canvas.height/2 -12);
					ctx.fillText('Press space bar to start again!', canvas.width/2 -135, canvas.height/2 +8);
				clearInterval(gameLoop2);
			}
			
			function lostLife(){
				clearInterval(gameLoop2);
				lives--;
				paused = true; 
				if(lives <= 0){
					game.over();
					paused = false;
				} else {
					ball.posX = 300;
					ball.posY = 240;
					
					ball.dx =4;
					ball.dy = 4;
					
					ctx.fillStyle = "black";
					ctx.fillRect(canvas.width/2 -150 , canvas.height/2 -37 , 300, 75);
					
					ctx.fillStyle = 'white';
					ctx.fillText('Press spacebar to continue', canvas.width/2 - 135, canvas.height/2 -12);
					ctx.fillText(lives + ' lives remaining', canvas.width/2 -135, canvas.height/2 +8);					
				}				
			}
			
			function gameWin(){
				clearInterval(gameLoop2);
			}		
		}
		
		function animateGame(){
			//createBricks();
			updatePaddle();
			updateBall();
			drawScoreboard();
		}
		
		/*function initGraphics(){
			paddleImg.onload = function(){
				//ctx.drawImage(paddleImg, 0,0);
			};
			paddleImg.src = "images/breakout/paddle.png";
		}*/
		
	} else {
		console.log(this.name + " is NOT active");
	}
}

function module5Clear(){
	console.log(this.name + " cleared.");
	clearInterval(gameLoop2);
	ctx.clearRect(0, 0, canvas.width, canvas.height);
}