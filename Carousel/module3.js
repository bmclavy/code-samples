var module3 = new contentModule("3 - Multi-Step Process Example", module3Content, module3Clear);
var infoID = -1;

var step1Header = "Step 1 -Kick Down the Door!";
var step1Desc = " Draw one card from the Door deck and turn it face up. If it’s a monster, you must fight it. See Combat, p.3. If the card is a curse – see Curses, p. 5 – it applies to you immediately (if it can) and is discarded (unless it has a persistent effect or you keep the card as a reminder of an upcoming effect). If you draw any other card, you may either put it in your hand or play it immediately.";
var step1img = "step1Image"
var s1infoObj1 = new infoObj("warning", "You've encountered a monster! Your level must be greater than that of the monster in order to defeat it.", [102, 42]);
var s1infoObj2 = new infoObj("info", "If you don't encounter a monster you may loot the room.", [12,20]);
var s1Objs = [s1infoObj1, s1infoObj2];
var step1 = new stepObj(step1Header, step1Desc, step1img, s1Objs);

var step2Header = "Step 2 - Fight the monster";
var step2Desc = "To fight a monster, compare its strength to yours. Combat strength is the total of Level plus all modifiers given by Items and other cards. If the monsters combat strength is equal to yours, or greater, you LOSE and must Run Away. If your combat strength totals more than the monsters you KILL it and go up a level.";
var step2img = "step2Image"
var s2infoObj1 = new infoObj("info", "Use items to augment your level.", [36, 65]);
var s2infoObj2 = new infoObj("warning", "Caution, your so-called 'friends' might backstab you to prevent you from defeating the monster!", [85, 21]);
var s2Objs = [s2infoObj1, s2infoObj2];
var step2 = new stepObj(step2Header, step2Desc, step2img, s2Objs);

var step3Header = "Step 3 - Grab the treasure";
var step3Desc = "When you kill a monster, you get one level per monster, unless the Monster card says something else, and you get all its Treasure! Each monster has a Treasure number on the bottom of its card. Draw that many Treasures, modified by any Monster Enhancers played on it. Draw face-down if you killed the monster alone.";
var step3img = "step3Image"
var step3 = new stepObj(step3Header, step3Desc, step3img );

var step4Header = "Step 4 - Reach level 10 and WIN";
var step4Desc = "The objective is simple; The first player to reach Level 10 wins . . . but you must reach Level 10 by killing a monster, unless a card specifically allows you to win another way. Get the official instructions <a href='http://www.worldofmunchkin.com/rules/munchkin_rules.pdf'>here</a>";
var step4img = "step4Image"
var s4infoObj1 = new infoObj("info", "Studies have shown that 8.4 out of 9.7 Munchkin players just can’t get enough of the game.", [104, 50]);
var s4Objs = [s4infoObj1];
var step4 = new stepObj(step4Header, step4Desc, step4img, s4Objs);

var steps = [step1, step2, step3, step4];
var activeStep = 0;

function module3Content(){
	if(this.active){
		$("#overlay").append("<div id='stepTitle'></div>");
		$("#stepTitle").css({"position":"absolute", "top":"45px", "left":"15px", "font-family": "Verdana, sans-serif", "font-size": "18px", "font-weight":"bold"});
		$("#overlay").append("<div id='imageDiv'></div>");
		$("#imageDiv").css({"position":"absolute", "top":"85px", "left":"145px", "width": "635px", "height":"185px"});
		
		$("#imageDiv").append("<div id ='step1image' data-pos = 0 class='stepImg'><img src = 'images/step1Image_grey.png' /></div>");		
		$("#step1image").css({"position":"absolute", "left":"0px", "z-index":"4", "width":"150px", "height":"150px", "margin-top":"auto", "margin-bottom":"auto"});
		
		$("#imageDiv").append("<div id ='step2image' data-pos = 1 class='stepImg'><img src = 'images/step2Image_grey.png' /></div>");
		$("#step2image").css({"position":"absolute", "left":"115px", "z-index":"3", "width":"150px", "height":"150px", "margin-top":"auto", "margin-bottom":"auto"});
		
		$("#imageDiv").append("<div id ='step3image' data-pos = 2 class='stepImg'><img src = 'images/step3Image_grey.png' /></div>");
		$("#step3image").css({"position":"absolute", "left":"230px", "z-index":"2", "width":"150px", "height":"150px", "margin-top":"auto", "margin-bottom":"auto"});
		
		$("#imageDiv").append("<div id ='step4image' data-pos = 3 class='stepImg'><img src = 'images/step4Image_grey.png' /></div>");
		$("#step4image").css({"position":"absolute", "left":"345px", "z-index":"1","width":"150px", "height":"150px", "margin-top":"auto", "margin-bottom":"auto"});
		
		$("#overlay").append("<div id='processFooter'><div id='description'></div></div>");
		$("#processFooter").css({"position":"absolute", "top":canvas.height-64, "left":"0px", "width": canvas.width-19,"height":"75px", "background-color":"black", "padding":"10px","font-family": "Verdana, sans-serif", "font-size": "14px", "color":"white"});
		
		$("#processFooter").append("<div id='buttonBar'></div>");
		$("#buttonBar").css ({"position":"absolute", "bottom":"5px", "height":"30px", "width":canvas.width-19});
		$("#buttonBar").append("<div id='nextButton_bb' class='button'></div>");
		$("#nextButton_bb").css({"width":"88px", "height":"30px","float":"right", "background-image": "url('images/next_gold.png')", "background-repeat": "no-repeat", "background-size": "cover"});
		$("#nextButton_bb").click(function(){
			swapStep("next");			
		});
		$("#buttonBar").append("<div id='prevButton_bb' class='button'></div>");
		$("#prevButton_bb").css({"width":"88px", "height":"30px","float":"left", "background-image": "url('images/prev_gold.png')", "background-repeat": "no-repeat", "background-size": "cover"});
		$("#prevButton_bb").click(function(){
			swapStep("prev");
		});
		
		displayStep(0);		
	}else{
		console.log (this.name + "is NOT active");
	}
}

function stepObj(thisName, thisDesc, thisImage, addInfo){
	this.name = thisName;
	this.desc = thisDesc;
	this.image = thisImage+".png";
	this.imageGrey = thisImage+"_grey.png";
	this.info = addInfo;
}

function infoObj(thisType, thisMsg, thisPos){
	infoID ++;
	this.name = infoID;
	this.type = thisType;
	this.msg = thisMsg;
	this.pos = thisPos;
}

function swapStep(swapWith){
	console.log("swapping" + swapWith);
	if (swapWith == "prev"){
		getStep = activeStep - 1;
		if (getStep < 0){
			getStep = 0;
		}
	} else if (swapWith == "next"){
		getStep = activeStep + 1;
		if(getStep > steps.length-1){
			getStep = steps.length-1;
		}
	}
	displayStep(getStep);
}

function displayStep(thisStep){
	activeStep = thisStep;
	$("#stepTitle").html(steps[activeStep].name);
	$("#description").html(steps[activeStep].desc);
	
	restoreStep($("#step1image"));
	restoreStep($("#step2image"));
	restoreStep($("#step3image"));
	restoreStep($("#step4image"));
	
	if(activeStep == 0){
		setupActiveStep($("#step1image"));		
	} else if(activeStep == 1){
		setupActiveStep($("#step2image"));
	} else if(activeStep == 2){
		setupActiveStep($("#step3image"));
	} else if(activeStep == 3){
		setupActiveStep($("#step4image"));;
	}
	
	if (activeStep >= steps.length-1){
		$("#nextButton_bb").hide();
	}else{
		$("#nextButton_bb").show();
	}
	if (activeStep <= 0){
		$("#prevButton_bb").hide();
	}else{
		$("#prevButton_bb").show();
	}	
}

function setupActiveStep(thisStepImage){
	if(intervalObj !== undefined){
		clearInterval(intervalObj);	
	}
	thisStepImage.html("<img src = 'images/" + steps[thisStepImage.attr("data-pos")].image + "' />");
	thisStepImage.css({"top":"0px", "height":"185px", "width":"185px"});
	if(	steps[activeStep].info !== undefined){
		for(var i = 0; i < steps[activeStep].info.length; i++){
			var headerColour;
			//var stepInfoID = "info"+ steps[activeStep].info[i].name;
			if (steps[activeStep].info[i].type == "warning"){
				imagePath = "images/warning.png";
				headerColour = "rgba(128,6,6,.85)";
				
			} else {
				imagePath = "images/info.png";
				headerColour = "rgba(39,54, 145,.85)";
			}
			
			thisStepImage.append("<div id='info" + i + "' class='button' data-pos =" + i + "></div> <div id='desc" + i + "'><div style='background-color: "+ headerColour +"; color: white; font-family: verdana, arial, sans-serif; padding-left: 7px;'>"+ steps[activeStep].info[i].type + "</div> <div style='color: white; font-size: 12px; padding: 5px; font-family: verdana, arial, sans-serif;'>" + steps[activeStep].info[i].msg + "</div> </div>");
				
			$("#info"+ i).css({"width":"25px", "height":"25px", "position":"absolute", "left":steps[activeStep].info[i].pos[0], "top":steps[activeStep].info[i].pos[1], "background-image":"url('"+imagePath+"')", "background-repeat": "no-repeat", "background-size": "cover", "z-index":"6", "display":"none"});
			$("#desc" +i).css({"position":"absolute", "top":steps[activeStep].info[i].pos[1]+1, "left": steps[activeStep].info[i].pos[0]+16, "width":"150px", "height":"100px", "background-color":"rgba(0,0,0,.85)", "display":"none"});
			$("#info"+ i).show('scale');
			var infoOpen = false;
			$("#info"+ i).hover(function(e){
				toggleInfo($(this));
			},
				function(e){
					toggleInfo($(this));

			});
			
			function toggleInfo(thisElement){
				$("#desc" + thisElement.attr("data-pos")).slideToggle( "fast" );
				if(!infoOpen){
					infoOpen = true;
					thisElement.css("z-index", 100);
					$("#desc" + thisElement.attr("data-pos")).css("z-index", 99);
				} else {
					infoOpen = false;
					thisElement.css("z-index", 6);
					$("#desc" + thisElement.attr("data-pos")).css("z-index", 5);
				}
			}
		}
		
		var thisObj = 0;
		intervalObj = setInterval(function(){
			if(thisObj >= steps[activeStep].info.length){
				thisObj = 0;
			}
			console.log("pulsing");
			pulse($("#info" + thisObj));
			thisObj++;
		}, 5000);
		
		function pulse(thisElement){
			console.log(thisElement);
			thisElement.animate({
				width: '28px'
				}, 500, function (){
				thisElement.animate({
					width: '25px'
					}, 500)
				});
		}
	}
}

function restoreStep(thisImage){
	thisImage.html("<img src = 'images/" + steps[thisImage.attr("data-pos")].imageGrey + "' />");
	thisImage.css({"top":"17.5px", "height":"150px", "width":"150px"});
	/*
	thisImage.mouseenter(function(e){
		e.stopPropagation();
		//console.log($(this).name +": over");
		thisImage.clearQueue().html("<img src = 'images/" + steps[thisImage.attr("data-pos")].image + "' />");
		thisImage.clearQueue().css({"top":"0px", "height":"185px", "width":"185px"});
	});
	
	thisImage.mouseleave(function(e){
		console.log("off");
		thisImage.html("<img src = 'images/" + steps[thisImage.attr("data-pos")].imageGrey + "' />");
		thisImage.css({"top":"17.5px", "height":"150px", "width":"150px"});
	});
	*/
}

function module3Clear(){
	$("#overlay").html("");
		if(intervalObj !== undefined){
		clearInterval(intervalObj);	
	}
}