var module2 = new contentModule("2 - Canvas & Video", module2Content, module2Close);
var videoSource = "<source src='media/video.mp4' type='video/mp4'>";
var vidWidth;
var vidHeight;
var intervalObj;

var vidStatement1 = "Text is displayed based on current time in video.";
var vidStatement2 = "Text independent of video.";
var vidStatement3 = "Update even when play head is moved back or forward.";
	
function module2Content(){	
	if(this.active){
		vidWidth = (canvas.height/1080) * 1920;
		vidHeight = canvas.height;
		ctx.fillStyle = "#000000";
		ctx.fillRect(0,0, 200, canvas.height);
		console.log(this.name + " is ACTIVE");
		videoElement.width = vidWidth;
		videoElement.height = vidHeight;
		ctx.fillStyle = "#ffffff";
		ctx.font = "bold 18px Verdana";
		ctx.fillText("Video header", 10, 25);
		
		$("#videoPlayer").html(videoSource);
		$("#videoPlayer").fadeIn(250);
		$("#videoPlayer").css({
			"top": 31, 
			"left": canvas.width - vidWidth + 1
		});
		
		videoElement.addEventListener("playing", captionHandler, false);
		videoElement.addEventListener("pause", stopInterval, false);
		videoElement.addEventListener("ended", stopInterval, false);

	} else {
		console.log(this.name + " is NOT active");
	}
}

function module2Close(){
	$("#videoPlayer").fadeOut(100,removeVideo);	
	stopInterval();
}

function removeVideo(){
	$("#videoPlayer").html("");
}

function stopInterval(e){
	clearInterval(intervalObj);	
}

function captionHandler(e){
	if(e){
		var check1 = false, check2 = false, check3 = false;
		var currentPos = 95;
		var updatePos = 75;
		var textWidth = (canvas.width - vidWidth - 10)
		ctx.fillStyle = "#000000";
		ctx.fillRect(0,0, 200, canvas.height);
		ctx.fillStyle = "#ffffff";
		ctx.font = "bold 18px Verdana";
		ctx.fillText("Video header", 10, 25);
		ctx.font = fontSize + " Verdana";
		
		checkCaptions();
		
		intervalObj = setInterval(function(){
			checkCaptions();
		}, 1000);
		console.log("Playing");
		
		function checkCaptions(){
			console.log("checking video: " + fontSpace);
			if(videoElement.currentTime >= 3 && !check1){
				wrapText(ctx, vidStatement1, 10, currentPos, textWidth, fontSpace);
				currentPos += updatePos;
				console.log("Add statement 1 " + check1);
				check1 = true;
			}
			
			if(videoElement.currentTime >= 6 && !check2){
				wrapText(ctx, vidStatement2, 10, currentPos, textWidth, fontSpace);
				currentPos += updatePos;
				console.log("Add statement 2 " + check2);
				check2 = true;
			}
			
			if(videoElement.currentTime >= 9 && !check3){
				wrapText(ctx, vidStatement3, 10, currentPos, textWidth, fontSpace);
				console.log("Add statement 3 " + check3);
				check3 = true;
			}			
		}
	}	
}

function checkText(){
	
}