var module6 = new contentModule("6 - Unity Web Player", module6Content, module6Clear);

function module6Content(){
	if(module6.active){
		var unityObjectUrl = "http://webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/UnityObject2.js";
		
			var config = {
				width: 800, 
				height: 350,
				params: { enableDebugging:"0" }
				
			};
			
			var u = new UnityObject2(config);

			$(function() {
				var $missingScreen = $("#overlay").find(".missing");
				var $brokenScreen = $("#overlay").find(".broken");
				$missingScreen.hide();
				$brokenScreen.hide();
				
				u.observeProgress(function (progress) {
					switch(progress.pluginStatus) {
						case "broken":
							$brokenScreen.find("a").click(function (e) {
								e.stopPropagation();
								e.preventDefault();
								u.installPlugin();
								return false;
							});
							$brokenScreen.show();
						break;
						case "missing":
							$missingScreen.find("a").click(function (e) {
								e.stopPropagation();
								e.preventDefault();
								u.installPlugin();
								return false;
							});
							$missingScreen.show();
						break;
						case "installed":
							$missingScreen.remove();
						break;
						case "first":
						break;
					}
				});
				u.initPlugin($("#overlay")[0], "webBuild.unity3d");
			});
		
	} else {

	}
}

function module6Clear(){
	$("#overlay").html("");
}