var module1 = new contentModule("1 - Introduction", module1Content);
var lineHeight = 18;
var bodyText1 = "The idea for the 'The Carousel' was to create a space where I couls explore different facets of web development, specifically in the realms of HTML 5, CSS, and Javascript... And, really, who knows what else!";
var bodyText2 = "Today will be my first foray into the <canvas> element. Essentially this is a digital canvas where objects are painted, on a pixel level, on to a canvas.  A vital component for creating HTML 5 games.";
var bodyText3 = "Beyond that I don't know what direction the Carousel will go, only that I suspect that Google will lead the way!";

function module1Content(){
	if(module1.active){
		var carouselImageObj = new Image();
		
		carouselImageObj.onload = function(){
			ctx.drawImage(carouselImageObj, 10, 55);
		}
		carouselImageObj.src = "images/carouse_icon96l.png";

		ctx.font = "bold 18px Verdana";
		ctx.fillStyle = "#000000";
		ctx.fillText("The 'Exploration' Carousel: <canvas>", 10, 25);
		ctx.font = "14px Verdana";
		wrapText(ctx, bodyText1, 125, 55, 670, lineHeight);
		
		wrapText(ctx, bodyText2, 125, 125, 670, lineHeight);
		
		wrapText(ctx, bodyText3, 15, 195, 670, lineHeight);
		var imageObj = new Image();
		
		
		imageObj.onload = function(){
			ctx.drawImage(imageObj, canvas.width - 210, canvas.height - 82.5, 200, 72.5);
		}
		imageObj.src = "images/800px-Google.png";
		
	} else {
		console.log(this.name + " is NOT active");
	}
}