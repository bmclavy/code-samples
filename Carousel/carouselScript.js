var modules = [module1, module2, module3, module4, module5, module6];
var activeModule = 4;

$(document).ready(function(){
	openModule(activeModule);
});

function swapModule(swapWith){
	console.log("swapping" + swapWith);
	if (swapWith == "prev"){
		getModule = activeModule - 1;
		if (getModule < 0){
			getModule = modules.length-1;
		}
	} else if (swapWith == "next"){
		getModule = activeModule + 1;
		if(getModule > modules.length-1){
			getModule = 0;
		}
	}
	openModule(getModule);
}

function openModule (thisModule){
	if(modules[thisModule] !== undefined){
		modules[activeModule].active = false;
		if(	modules[activeModule].clear != undefined){
			modules[activeModule].clear();
			ctx.clearRect(0, 0, canvas.width, canvas.height);
		}
		activeModule = thisModule;
		$("#moduleTitle").html(modules[activeModule].name);
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		modules[activeModule].active = true;
		modules[activeModule].content();	
	}
}
