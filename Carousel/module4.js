var module4 = new contentModule("4 - Game Time", module4Content, module4Clear);
var gameLoop;
function module4Content(){
	if(module4.active){
		var score = 0;
		
		var bricksPerRow = 8;                               
		var brickHeight = 20;
		var brickWidth = canvas.width/bricksPerRow;
				
		var paddleX = 200;
		var paddleY = canvas.height-45; 
		var paddleWidth = 100;
		var paddleHeight = 15; 
		var paddleDeltaX = 0;
		var paddleDeltaY = 0;
		var paddleSpeedX = 10;
		var paddleMove;
		
		var ballX = 300;
		var ballY = 240;
		var ballRadius = 10;
		var ballDeltaX;
		var ballDeltaY;
 
		function drawPaddle(){
		    ctx.fillRect(paddleX,paddleY,paddleWidth,paddleHeight);
		}
		
		function movePaddle(){
			if (paddleMove == 'LEFT'){
				paddleDeltaX = -paddleSpeedX;
			} else if (paddleMove == 'RIGHT'){
				paddleDeltaX = paddleSpeedX;
			} else {
				paddleDeltaX = 0;
			}
			
			// If paddle reaches the side of the screen, then don't let it move any further
			if (paddleX + paddleDeltaX < 0 || paddleX + paddleDeltaX +paddleWidth >canvas.width){
				paddleDeltaX = 0; 
			}
			
			paddleX = paddleX + paddleDeltaX;
		}
 
		function drawBall(){
			ctx.beginPath();
			ctx.arc(ballX,ballY,ballRadius,0,Math.PI*2,true);
			ctx.fill();			 
		}
		
		function moveBall(){	
			// Top check
			if (ballY + ballDeltaY - ballRadius < 0 || collisionYWithBricks()){
				ballDeltaY = -ballDeltaY;
			}
		
			// Bottom check = endgame
			if (ballY + ballDeltaY + ballRadius > canvas.height){
				endGame();
			}
		
			// Side Checks
				//left of ball moves too far left      or   right side of ball moves too far right
			if ((ballX + ballDeltaX - ballRadius < 0) || (ballX + ballDeltaX + ballRadius > canvas.width) || collisionXWithBricks()){  
				ballDeltaX = -ballDeltaX;
			}
			
			if(ballY + ballDeltaY + ballRadius >= paddleY){
				if(ballX+ballDeltaX >= paddleX && ballX + ballDeltaX <= paddleX+paddleWidth){
					ballDeltaY =- ballDeltaY;
				}
			}
		
			// Move the ball
			ballX = ballX + ballDeltaX;
			ballY = ballY + ballDeltaY;
		}
		
		// Brick Layout: 1 is orange, 2 is green, 3 is gray, 0 means no brick 
		var bricks = [
			[1,1,1,1,1,1,1,2],
			[1,1,3,1,0,1,1,1],
			[2,1,2,1,2,1,0,1],
			[1,2,1,1,0,3,1,1]
		];


		// draw each brick using drawBrick()
		function createBricks(){
			for (var i=0; i < bricks.length; i++) {
				for (var j=0; j< bricks[i].length; j++) {
					drawBrick(j,i,bricks[i][j]);
				}
			}
		}
		
		function drawBrick(x,y,type){   
			switch(type){ // set brick colour if visible, clear if not.
				case 1:
					ctx.fillStyle = 'orange';
					break;
				case 2:
					ctx.fillStyle = 'rgb(100,200,100)';                     
					break;
				case 3:
					ctx.fillStyle = 'rgba(50,100,50,.5)';
					break;                              
				default:
					ctx.clearRect(x*brickWidth,y*brickHeight,brickWidth,brickHeight);
					break;
		
			}
			if (type){
				//Draw rectangle with fillStyle color selected earlier w/ border
				ctx.fillRect(x*brickWidth,y*brickHeight,brickWidth,brickHeight);
				ctx.strokeRect(x*brickWidth+1,y*brickHeight+1,brickWidth-2,brickHeight-2);
			} 
		}
		
		function collisionXWithBricks(){
			var bumpedX = false;
			
			for (var i=0; i < bricks.length; i++) {
				for (var j=0; j < bricks[i].length; j++) {
					if (bricks[i][j]){ // if brick is still visible
						var brickX = j * brickWidth;
						var brickY = i * brickHeight;
						
						if ((
						(ballX + ballDeltaX + ballRadius >= brickX) 
						&& 
						(ballX + ballRadius <= brickX)) 
						||
						((ballX + ballDeltaX - ballRadius <= brickX + brickWidth) 
						&&
						(ballX - ballRadius >= brickX + brickWidth)))
						{      
							if ((ballY + ballDeltaY -ballRadius <= brickY + brickHeight) 
							&&
							(ballY + ballDeltaY + ballRadius >= brickY))
							{
								explodeBrick(i,j);		 
								bumpedX = true;
							}
						}
					}
				}
    		}
	        return bumpedX;
		}
		
		function collisionYWithBricks(){
			var bumpedY = false;
			for (var i=0; i < bricks.length; i++) {
				for (var j=0; j < bricks[i].length; j++) {
					if (bricks[i][j]){ // if brick is still visible
						var brickX = j * brickWidth;
						var brickY = i * brickHeight;
				        if ((
						(ballY + ballDeltaY - ballRadius <= brickY + brickHeight) 
						&& 
						(ballY - ballRadius >= brickY + brickHeight))
						||
						((ballY + ballDeltaY + ballRadius >= brickY)
						&&
						(ballY + ballRadius <= brickY )))
						{
							if (ballX + ballDeltaX + ballRadius >= brickX 
							&& 
							ballX + ballDeltaX - ballRadius <= brickX + brickWidth)
							{                                  
								explodeBrick(i,j);                          
								bumpedY = true;
							}                       
						}
					}
				}
			}
			return bumpedY;
		}
		
		function explodeBrick(i,j){
			bricks[i][j] --;
					
			if (bricks[i][j]>0){ 
				score++;
			} else {
				score += 2;     
			}
		}

		 
		function displayScoreBoard(){
			ctx.fillStyle = 'rgb(50,100,50)';
			ctx.font = "20px Times New Roman";
			
			ctx.clearRect(0,canvas.height-30,canvas.width,30);  
			ctx.fillText('Score: '+score,10,canvas.height-5);
		}
		
		function animate(){
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			
			createBricks();
			displayScoreBoard();
			moveBall();
			movePaddle();
			drawPaddle();
			drawBall();
		}
		
		function startGame(){
			ballDeltaY = -4;
			ballDeltaX = -2;
			
			paddleMove = 'NONE';
			paddleDeltaX = 0;
			
			gameLoop = setInterval(animate, 16);
			
			$(document).keydown(function(evt) {
				if (evt.keyCode == 39) {
					paddleMove = 'RIGHT';
				} else if (evt.keyCode == 37){
					paddleMove = 'LEFT';
				}
			});         
		 
			$(document).keyup(function(evt) {
				if (evt.keyCode == 39) {
					paddleMove = 'NONE';
				} else if (evt.keyCode == 37){
					paddleMove = 'NONE';
				}
			});
		}
		
		function endGame(){
			clearInterval(gameLoop);
			ctx.fillText('The End!',canvas.width/2,canvas.height/2);
		}
		
		startGame();
		drawPaddle();
		drawBall();
		
		createBricks();
		displayScoreBoard();
		
	} else {
		console.log(this.name + " is NOT active");
	}
}

function module4Clear(){
	console.log(this.name + " cleared.");
	clearInterval(gameLoop);
}