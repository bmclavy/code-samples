var ctx;
var canvas;
var videoElement;
var fontSize = "14px";
var fontSpace = parseInt(fontSize.slice(0,fontSize.length-2)) + parseInt(fontSize.slice(0,fontSize.length-2))/3;

$(document).ready(function(){
	canvas = document.getElementById("mainCanvas");
	videoElement = document.getElementById("videoPlayer");
	ctx = canvas.getContext("2d");
	
	$("#nextButton").click(function(){
		swapModule("next");
	});
	$("#prevButton").click(function(){
		swapModule("prev");
	});
});

	function contentModule(thisTitle, thisContent, thisClear){
		this.name = thisTitle;
		this.active = false;
		this.content = thisContent;
		this.clear = thisClear;
	}
	
	function wrapText(context, text, x, y, maxWidth, lineHeight) {
        var words = text.split(' ');
        var line = '';

        for(var n = 0; n < words.length; n++) {
          var testLine = line + words[n] + ' ';
          var metrics = context.measureText(testLine);
          var testWidth = metrics.width;
          if (testWidth > maxWidth && n > 0) {
            context.fillText(line, x, y);
            line = words[n] + ' ';
            y += lineHeight;
          }
          else {
            line = testLine;
          }
        }
        context.fillText(line, x, y);
      }